from distutils.core import setup, Extension

module1 = Extension('stations',
                    sources = ['stationsmodule.c'])

setup (name = 'PackageName',
       version = '1.0',
       description = 'Python Module example for perfect-hash',
       ext_modules = [module1])
